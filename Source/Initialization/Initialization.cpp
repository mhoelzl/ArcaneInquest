// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#include "Initialization.h"
#include "EngineGlobals.h"
// This is required for the AbilitySystemGlobals.h include to work since there is an include chain AbilitySystemGlobals.h -> GameplayTagsModule.h -> GameplayTagsManager.h which uses FTableRowBase.
#include "Engine/DataTable.h"
#include "AbilitySystemGlobals.h"
#include "GameplayAbilitiesModule.h"

IMPLEMENT_GAME_MODULE(FInitializationModule, Initialization);

DEFINE_LOG_CATEGORY(LogInitialization)

void FInitializationModule::StartupModule()
{
	FDefaultGameModuleImpl::StartupModule();

	UE_LOG(LogInitialization, Log, TEXT("Starting Initialization module."));

	IGameplayAbilitiesModule& GameplayAbilitiesModule{ IGameplayAbilitiesModule::Get() };
	if (ensure(GameplayAbilitiesModule.IsAvailable()))
	{
		UAbilitySystemGlobals* AbilitySystemGlobals{ GameplayAbilitiesModule.GetAbilitySystemGlobals() };
		if (AbilitySystemGlobals && !AbilitySystemGlobals->IsAbilitySystemGlobalsInitialized())
		{
			AbilitySystemGlobals->InitGlobalData();
		}
	}
}

void FInitializationModule::ShutdownModule()
{
	FDefaultGameModuleImpl::ShutdownModule();
	UE_LOG(LogInitialization, Log, TEXT("Shuting down Initialization module."))
}
