// Copyright 2016 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "EngineMinimal.h"
#include "ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(LogInitialization, All, All);

class INITIALIZATION_API FInitializationModule : public FDefaultGameModuleImpl
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
