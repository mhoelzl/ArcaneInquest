// Copyright 2016 Matthias H�lzl, All Rights Reserved.

using UnrealBuildTool;

public class Initialization : ModuleRules
{
	public Initialization(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule", "GameplayTags", "GameplayTasks", "GameplayAbilities", "AssetProvider" });
	}
}
