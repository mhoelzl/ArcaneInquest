// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "EngineMinimal.h"
#include "EngineGlobals.h"
#include "ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(LogArcaneInquest, All, All);

// We no longer need a custom game module, since loading of the gameplay tags and ability modules is more correctly handled in the custom engine. 
// Leave the module here for now, in case we have to perform some work later on.
class FArcaneInquestModule : public FDefaultGameModuleImpl
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
