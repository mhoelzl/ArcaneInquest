// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

using UnrealBuildTool;

public class ArcaneInquest : ModuleRules
{
	public ArcaneInquest(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "GameplayTags", "AIModule", "GameplayTasks", "GameplayAbilities", "UMG", "AssetProvider" });
        PrivateDependencyModuleNames.AddRange(new string[] { "SlateCore", "Slate" });
    }
}
