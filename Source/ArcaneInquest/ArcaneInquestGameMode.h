// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once
#include "GameFramework/GameMode.h"
#include "ArcaneInquestGameMode.generated.h"

UCLASS(minimalapi)
class AArcaneInquestGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AArcaneInquestGameMode();
};



