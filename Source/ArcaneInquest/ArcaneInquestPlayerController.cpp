// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "Units/AbilityCharacter.h"
#include "GameplayAbilityTypes.h"
#include "AbilitySystemComponent.h"
#include "ArcaneInquestPlayerController.h"

AArcaneInquestPlayerController::AArcaneInquestPlayerController()
	: Super()
{
	InputComponent = CreateDefaultSubobject<UInputComponent>("InputComponent");
	check(InputComponent);
}

void AArcaneInquestPlayerController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	BindMovementKeys(InPawn);

	// If Pawn is an AAbilityCharacter, provide bindings for activating the
	// abilities.
	BindAbilityKeys(InPawn);

}

void AArcaneInquestPlayerController::SetGenericTeamId(const FGenericTeamId& InTeamID)
{
	TeamID = InTeamID;
	if (GetPlayerCharacter() && GetPlayerCharacter()->GetGenericTeamId() != InTeamID)
	{
		GetPlayerCharacter()->SetGenericTeamId(InTeamID);
	}
}

void AArcaneInquestPlayerController::BindMovementKeys(APawn* InPawn)
{
	AAbilityCharacter* AbilityCharacter{ Cast<AAbilityCharacter>(InPawn) };
	// We could bind some movements for characters that are not AAbilityCharacters, but it's probably not worth the effort. However we ensure that we have an ability character, otherwise people might become very confused when their pawn doesn't move.

	if (ensure(AbilityCharacter))
	{
		check(InputComponent);
		// InputComponent->BindAction("Jump", IE_Pressed, AbilityCharacter, &ACharacter::Jump);
		// InputComponent->BindAction("Jump", IE_Released, AbilityCharacter, &ACharacter::StopJumping);
		InputComponent->BindAction("PrintDebugMessage", IE_Pressed, AbilityCharacter, &AAbilityCharacter::PrintDebugMessage);

		InputComponent->BindAction("Sprint", IE_Pressed, AbilityCharacter, &AAbilityCharacter::StartSprinting);
		InputComponent->BindAction("Sprint", IE_Released, AbilityCharacter, &AAbilityCharacter::StopSprinting);

		InputComponent->BindAction("ToggleRun", IE_Pressed, AbilityCharacter, &AAbilityCharacter::ToggleRunning);

		InputComponent->BindAxis("MoveForward", AbilityCharacter, &AAbilityCharacter::MoveForward);
		InputComponent->BindAxis("MoveRight", AbilityCharacter, &AAbilityCharacter::MoveRight);

		// We have 2 versions of the rotation bindings to handle different kinds of devices differently
		// "turn" handles devices that provide an absolute delta, such as a mouse.
		// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
		InputComponent->BindAxis("Turn", AbilityCharacter, &APawn::AddControllerYawInput);
		InputComponent->BindAxis("TurnRate", AbilityCharacter, &AAbilityCharacter::TurnAtRate);
		InputComponent->BindAxis("LookUp", AbilityCharacter, &APawn::AddControllerPitchInput);
		InputComponent->BindAxis("LookUpRate", AbilityCharacter, &AAbilityCharacter::LookUpAtRate);

		// Handle touch devices
		InputComponent->BindTouch(IE_Pressed, AbilityCharacter, &AAbilityCharacter::TouchStarted);
		InputComponent->BindTouch(IE_Released, AbilityCharacter, &AAbilityCharacter::TouchStopped);
	}
}

void AArcaneInquestPlayerController::BindAbilityKeys(APawn* InPawn)
{
	AAbilityCharacter* AbilityCharacter{ Cast<AAbilityCharacter>(InPawn) };
	if (AbilityCharacter)
	{
		check(InputComponent);
		UAbilitySystemComponent* AbilitySystemComponent{ AbilityCharacter->GetAbilitySystemComponent() };
		if (ensure(AbilitySystemComponent))
		{
			FGameplayAbiliyInputBinds InputBinds(FString(TEXT("AbilityConfirm")), FString(TEXT("AbilityCancel")), FString(TEXT("EGameplayAbilityInputBinds")) /*, 0, 1 */);
			AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, InputBinds);
		}
	}
}


