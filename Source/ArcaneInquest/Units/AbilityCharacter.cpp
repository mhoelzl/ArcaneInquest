// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "ArcaneInquestPlayerController.h"
#include "AssetProvider.h"
#include "DataTableFinder.h"
#include "UnitFinder.h"
#include "Engine/DataAsset.h"
#include "GameplayAbilitySet.h"
#include "GameplayTasksComponent.h"
#include "AbilityCharacter.h"


AAbilityCharacter::AAbilityCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, CombatStyle{ ECombatStyle::None }
	, BaseTurnRate{ 200.f }
	, BaseLookUpRate{ 200.f }
	, WalkSpeed{ 200.f }
	, RunSpeed{ 600.f }
	, SprintSpeed{ 1200.f }
	, bIsRunning{ true }
	, bIsSprinting{ false }
	, AbilitySystemComponent{ CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComponent") }
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AAbilityCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	UDataTable* InitData{ FAssetProviderModule::Get().GetDataTableFinder()->GetGenericAttributesDataTable() };
	const UAttributeSet* Attributes{ AbilitySystemComponent->InitStats(UGenericAttributeSet::StaticClass(), InitData) };
	GenericAttributes = CastChecked<const UGenericAttributeSet>(Attributes);

	UnitDataUpdated();
}

void AAbilityCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	SynchronizeWithControllerTeamId(NewController);
}

void AAbilityCharacter::SetGenericTeamId(const FGenericTeamId& NewTeamId)
{
	GenericTeamId = NewTeamId;
	SynchronizeWithControllerTeamId(GetController());
	UnitDataUpdated();
}

FGenericTeamId AAbilityCharacter::GetGenericTeamId() const
{
	return GenericTeamId;
}

#if WITH_EDITOR
void AAbilityCharacter::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UProperty* Property{ PropertyChangedEvent.Property };
	if (ensure(Property))
	{
		static const FString GenericTeamIdPropertyName{ FString(TEXT("TeamId")) };
		static const FString CombatStylePropertyName{ FString(TEXT("CombatStyle")) };
		FString Name{ Property->GetName() };
		if (Name == CombatStylePropertyName || Name == GenericTeamIdPropertyName)
		{
			UnitDataUpdated();
		}
	}
}
#endif /* WITH_EDITOR */

void AAbilityCharacter::UnitDataUpdated(bool bRefreshAbilities)
{
	const FUnitInitData& Data{ GetUnitFinder()->GetUnitData(GetUnitKind(), GenericTeamId, CombatStyle) };

	GetMesh()->SetSkeletalMesh(Data.Mesh);
	GetMesh()->SetRelativeLocation(Data.Location);
	GetMesh()->SetRelativeRotation(Data.Rotation);
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	GetMesh()->SetAnimInstanceClass(Data.AnimInstance);
	GetCapsuleComponent()->SetCapsuleSize(Data.CapsuleSize.X, Data.CapsuleSize.Y);

	if (Data.Material)
	{
		GetMesh()->SetMaterial(0, Data.Material);
	}

	if (bRefreshAbilities && Data.Abilities)
	{
		AbilitySystemComponent->ClearAllAbilities();
		Data.Abilities->GiveAbilities(AbilitySystemComponent);
	}

}

void AAbilityCharacter::SynchronizeWithControllerTeamId(AController* InController)
{
	if (!InController)
	{
		return;
	}

	// Push our own TeamId to the controller, if we are possessed by an AI controller
	AAIController* AIController{ Cast<AAIController>(InController) };
	if (AIController)
	{
		AIController->SetGenericTeamId(GenericTeamId);
		return;
	}

	// Reset our TeamId to the player controller's Id if we are controlled by a player
	auto* PlayerController{ Cast<AArcaneInquestPlayerController>(InController) };
	if (PlayerController)
	{
		GenericTeamId = PlayerController->GetGenericTeamId();
		return;
	}

	// Something is not as it should be.
	UE_LOG(LogArcaneInquest, Warning, TEXT("Don't know in which direction to synchronize the Team Ids."))
}

EUnitKind AAbilityCharacter::GetUnitKind() const
{
	return UnitKind;
}

class UUnitFinder* AAbilityCharacter::GetUnitFinder()
{
	static FAssetProviderModule& Module{ FAssetProviderModule::Get() };
	static UUnitFinder* Result{ Module.GetUnitFinder() };
	checkf(Result, TEXT("Could not retrieve unit finder."));
	return Result;
}

/// Movement
//  =========

void AAbilityCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// Jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AAbilityCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AAbilityCharacter::PrintDebugMessage()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Strength: %f"), GetStrength()));
	}
}

void AAbilityCharacter::TurnAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAbilityCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AAbilityCharacter::StartSprinting()
{
	UE_LOG(LogArcaneInquest, Log, TEXT("Starting to sprint."));
	bIsSprinting = true;
	SetMovementSpeed();
}

void AAbilityCharacter::StopSprinting()
{
	UE_LOG(LogArcaneInquest, Log, TEXT("Stopping to sprint."));
	bIsSprinting = false;
	SetMovementSpeed();
}

void AAbilityCharacter::ToggleRunning()
{
	UE_LOG(LogArcaneInquest, Log, TEXT("Switching between walking and running."));
	bIsRunning = !bIsRunning;
	SetMovementSpeed();
}

void AAbilityCharacter::SetMovementSpeed()
{
	float NewSpeed{ bIsSprinting ? SprintSpeed : (bIsRunning ? RunSpeed : WalkSpeed) };
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

void AAbilityCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// Find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// Get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// And we move...
		AddMovementInput(Direction, Value);
	}
}

void AAbilityCharacter::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
