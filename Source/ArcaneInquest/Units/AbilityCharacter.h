// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "AbilitySystemInterface.h"
#include "GameFramework/Character.h"
#include "GameplayCueInterface.h"
#include "GenericTeamAgentInterface.h"
#include "Stats/GenericAttributeSet.h"
#include "UnitCharacteristics.h"
#include "AbilityCharacter.generated.h"

UCLASS(Abstract)
class ARCANEINQUEST_API AAbilityCharacter : public ACharacter, public IGenericTeamAgentInterface, public IGameplayCueInterface, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AAbilityCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// Overridden to initialize ability system
	virtual void PostInitializeComponents() override;

	// Overridden to set the controller's TeamId when the pawn is possessed.
	virtual void PossessedBy(AController* NewController) override;

	virtual void SetCombatStyle(ECombatStyle InCombatStyle)
	{
		CombatStyle = InCombatStyle;
	};
	virtual ECombatStyle GetCombatStyle() const
	{
		return CombatStyle;
	};

	// IGenericTeamAgentInterface implementation
	virtual void SetGenericTeamId(const FGenericTeamId& NewTeamId) override;
	virtual FGenericTeamId GetGenericTeamId() const override;


#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	EUnitKind UnitKind;
	EUnitKind GetUnitKind() const;

	/**
	 * Update the data for the character based on the initial data for its kind and combat style. If bRefreshAbilities is true also replace its current abilities with the ones from the initial data.
	 */
	void UnitDataUpdated(bool bRefreshAbilities = true);

	/** 
	 * The combat style of this character. This influences its initial abilites.
	 * TODO: Add some gameplay tags based on the combat style of a character, so that various abilities may only be obtained by characters with a certain style.
	 * TODO: It would be even better to completely replace this enumeration with gameplay tags, but I'm not sure whether we run into any initialization-order problems when doing this. Investigate once the basic functionality works.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes", Meta = (AllowPrivateAccess = "true"))
	ECombatStyle CombatStyle;

	/** 
	 * The ability system component that keeps track of this character's abilities. Placed on the character, not the controller, so that players may possess different characters and obtain the correct abilities.
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ArcaneInquest", Meta = (AllowPrivateAccess = "true"))
	class UAbilitySystemComponent* AbilitySystemComponent;

	/** 
	 * The attributes all ability characters share.
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", Meta = (AllowPrivateAccess = "true"))
	const UGenericAttributeSet* GenericAttributes;

public:
	UFUNCTION(BlueprintPure, Category = "GenericAttributes")
	float GetMaxHealth() const
	{
		return GenericAttributes->MaxHealth;
	}

	UFUNCTION(BlueprintPure, Category = "GenericAttributes")
	float GetHealth() const
	{
		return GenericAttributes->Health;
	}

	UFUNCTION(BlueprintPure, Category = "GenericAttributes")
	float GetMaxMana() const
	{
		return GenericAttributes->MaxMana;
	}

	UFUNCTION(BlueprintPure, Category = "GenericAttributes")
	float GetMana() const
	{
		return GenericAttributes->Mana;
	}

	UFUNCTION(BlueprintPure, Category = "GenericAttributes")
	float GetStrength() const
	{
		return GenericAttributes->Strength;
	}

protected:
	void SynchronizeWithControllerTeamId(AController* InController);
	static class UUnitFinder* GetUnitFinder();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
	FGenericTeamId GenericTeamId;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, Meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, Meta = (AllowPrivateAccess = "true"))
	float BaseLookUpRate;

	/** The speed of the character when walking. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", Meta = (AllowPrivateAccess = "true"))
	float WalkSpeed;

	/** The speed of the character when running. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", Meta = (AllowPrivateAccess = "true"))
	float RunSpeed;

	/** The rate at which sprinting causes us to become faster. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", Meta = (AllowPrivateAccess = "true"))
	float SprintSpeed;

	/** Are we currently running? */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient, Category = "Movement", Meta = (AllowPrivateAccess = "true"))
	bool bIsRunning;

	/** Are we currently sprinting? */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient, Category = "Movement", Meta = (AllowPrivateAccess = "true"))
	bool bIsSprinting;

public:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Called when the character starts to sprint. Should set up the new movement speed. */
	void StartSprinting();

	/** Called when the character stops sprinting. Should restore the old movement speed. */
	void StopSprinting();

	/** Called whenever the character switches between walking and running. Should set the correct movement speed, taking into account sprinting. */
	void ToggleRunning();

	/** Sets the movement speed in the movement component, based on the flags for sprinting and running/walking. */
	void SetMovementSpeed();

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void PrintDebugMessage();

public:
	// Ability system interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override
	{
		return AbilitySystemComponent;
	}

};
