// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "AbilityCharacter.h"
#include "UnitCharacteristics.h"
#include "Minion.generated.h"

UCLASS()
class ARCANEINQUEST_API AMinion : public AAbilityCharacter
{
	GENERATED_BODY()

public:
	AMinion();

	UFUNCTION(BlueprintCallable, Category = "Combat")
	float GetMaxAttackRangeInCm() const;

	UFUNCTION(BlueprintCallable, Category = "Combat")
	virtual bool IsRanged() const;
};
