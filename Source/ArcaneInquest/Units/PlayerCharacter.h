// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once
#include "GameFramework/Character.h"
#include "AbilityCharacter.h"
#include "PlayerCharacter.generated.h"

UCLASS(config=Game)
class ARCANEINQUEST_API APlayerCharacter : public AAbilityCharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter();

protected:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

