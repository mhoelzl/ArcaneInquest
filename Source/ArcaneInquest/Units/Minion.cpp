// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "Animation/AnimInstance.h"
#include "AIController.h"
#include "AI/MinionAIController.h"
#include "UnitFinder.h"
#include "UnitCharacteristics.h"
#include "Minion.h"
#include <AssetProvider.h>


AMinion::AMinion()
	: Super()
{
	UnitKind = EUnitKind::Minion;

	// Make sure that we use the correct mesh and geometry when placing the character in the editor
	UnitDataUpdated(false);

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = AMinionAIController::StaticClass();
}

float AMinion::GetMaxAttackRangeInCm() const
{
	// TODO: This should depend on abilities and be defined in a data table...
	switch (CombatStyle)
	{
	case ECombatStyle::None:
		return -1.f;
	case ECombatStyle::Melee:
		return 150.f;
	case ECombatStyle::Ranged:
		return 1200.f;
	// We are a mage; attack from afar
	// Todo: Make this depend on the kind of mage we are
	default:
		return 1000.f;
	}
	// Can't happen.
	UE_LOG(LogArcaneInquest, Warning, TEXT("Unknown preferred combat range for minion?"));
	return 0.f;
}

bool AMinion::IsRanged() const
{
	// TODO: This should take into account the abilities of the character
	return false;
}
