// Copyright (c) 2016, Dr. Matthias Hölzl

#include "ArcaneInquest.h"
#include "Units/AbilityCharacter.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbility_ApplyEffect.h"




UGameplayAbility_ApplyEffect::UGameplayAbility_ApplyEffect(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::NonInstanced;
}

bool UGameplayAbility_ApplyEffect::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags /* = nullptr */, const FGameplayTagContainer* TargetTags /* = nullptr */, OUT FGameplayTagContainer* OptionalRelevantTags /* = nullptr */) const
{
	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}
	// TODO: Check whether the effect can be applied?
	return true;
}

void UGameplayAbility_ApplyEffect::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	if (EffectToApply && HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		AAbilityCharacter* Character{ Cast<AAbilityCharacter>(ActorInfo->AvatarActor.Get()) };
		UAbilitySystemComponent* ASC{ Character->GetAbilitySystemComponent() };

		FGameplayEffectContextHandle ContextHandle;
		ASC->ApplyGameplayEffectToSelf(EffectToApply, 1.f, ContextHandle);
	}
}
