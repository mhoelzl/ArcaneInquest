// Copyright (c) 2016, Dr. Matthias Hölzl

#pragma once

#include "Abilities/GameplayAbility.h"
#include "GameplayAbility_ApplyEffect.generated.h"

/**
 * A simple ability that applies a gameplay effect to the actor.
 */
UCLASS(Blueprintable)
class ARCANEINQUEST_API UGameplayAbility_ApplyEffect : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UGameplayAbility_ApplyEffect(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags /* = nullptr */, const FGameplayTagContainer* TargetTags /* = nullptr */, OUT FGameplayTagContainer* OptionalRelevantTags /* = nullptr */) const override;

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ability)
	UGameplayEffect* EffectToApply;
};
