// Copyright (c) 2016, Dr. Matthias Hölzl

#include "ArcaneInquest.h"
#include "GameplayAbility_BoostStrength.h"

UGameplayAbility_BoostStrength::UGameplayAbility_BoostStrength(const FObjectInitializer& ObjectInitializer /*= FObjectInitializer::Get()*/)
	: Super(ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UGameplayEffect> BoostStrengthFinder{ TEXT("Blueprint'/Game/ArcaneInquest/Abilities/Effect_BoostStrength.Effect_BoostStrength_C'") };
	if (ensure(BoostStrengthFinder.Succeeded()))
	{
		EffectToApply = Cast<UGameplayEffect>(BoostStrengthFinder.Class->GetArchetype());
		// ensure(EffectToApply);
	}
}
