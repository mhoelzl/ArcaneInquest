// Copyright (c) 2016, Dr. Matthias Hölzl

#pragma once

#include "Gameplay/Abilities/GameplayAbility_ApplyEffect.h"
#include "GameplayAbility_BoostStrength.generated.h"

/**
 * 
 */
UCLASS()
class ARCANEINQUEST_API UGameplayAbility_BoostStrength : public UGameplayAbility_ApplyEffect
{
	GENERATED_BODY()
	
public:
	UGameplayAbility_BoostStrength(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
};
