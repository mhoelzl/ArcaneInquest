// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "Units/PlayerCharacter.h"
#include "ArcaneInquestPlayerController.generated.h"

/**
 * The player controller for players with two actions and three abilities that are activated in a style similar to MOBAs. (Actions on left and right mouse buttons, abilities on Q, E and R. Cancel ability on C, sprint on left shift and toggle run/walk on left control key. Also, return to base on B but that should probably be revised.)
 */
UCLASS()
class ARCANEINQUEST_API AArcaneInquestPlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	AArcaneInquestPlayerController();

	UFUNCTION(BlueprintPure, Category = "ArcaneInquest")
	APlayerCharacter* GetPlayerCharacter()
	{
		return Cast<APlayerCharacter>(GetCharacter());
	};

	virtual void Possess(APawn* Pawn) override;

	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override;
	virtual FGenericTeamId GetGenericTeamId() const override
	{
		return TeamID;
	}

protected:
	FGenericTeamId TeamID;
	void BindMovementKeys(APawn* Pawn);
	void BindAbilityKeys(APawn* Pawn);
};
