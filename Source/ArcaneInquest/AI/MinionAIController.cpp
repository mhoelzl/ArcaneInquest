// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Units/Minion.h"
#include "MinionAIController.h"


AMinionAIController::AMinionAIController()
	: Super()
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinder<UBehaviorTree> BTFinder;

		FConstructorStatics()
			: BTFinder{TEXT("BehaviorTree'/Game/ArcaneInquest/AI/MinionBehaviorTree.MinionBehaviorTree'")}
		{
			ensure(BTFinder.Succeeded());
		};
	};
	static FConstructorStatics ConstructorStatics;

	if (ConstructorStatics.BTFinder.Succeeded())
	{
		BehaviorTree = ConstructorStatics.BTFinder.Object;
	}
}

void AMinionAIController::BeginPlay()
{
	Super::BeginPlay();
	if (ensure(BehaviorTree))
	{
		RunBehaviorTree(BehaviorTree);
		AMinion* ControlledMinion{ GetMinion() };
		if (ensure(ControlledMinion))
		{
			Blackboard->SetValueAsBool(IsRangedKey, ControlledMinion->IsRanged());
			Blackboard->SetValueAsFloat(AttackRangeKey, ControlledMinion->GetMaxAttackRangeInCm());
		}
	}
}

AMinion* AMinionAIController::GetMinion() const
{
	return Cast<AMinion>(GetPawn());
}

const FName AMinionAIController::IsRangedKey = FName(TEXT("IsRanged?"));
const FName AMinionAIController::AttackRangeKey = FName(TEXT("AttackRange"));
