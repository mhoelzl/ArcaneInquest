// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "AIController.h"
#include "MinionAIController.generated.h"

class AMinion;

/**
 * The AIController for minions.
 */
UCLASS()
class ARCANEINQUEST_API AMinionAIController : public AAIController
{
	GENERATED_BODY()

public:
	AMinionAIController();

	virtual void BeginPlay() override;
	
	/** The behavior tree used by this controller. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "Behavior")
	class UBehaviorTree* BehaviorTree;

	AMinion* GetMinion() const;

protected:
	// Blackboard keys
	static const FName IsRangedKey;
	static const FName AttackRangeKey;
};

