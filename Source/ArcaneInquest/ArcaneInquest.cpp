// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"

// IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ArcaneInquest, "ArcaneInquest" );
IMPLEMENT_PRIMARY_GAME_MODULE(FArcaneInquestModule, ArcaneInquest, "ArcaneInquest");

DEFINE_LOG_CATEGORY(LogArcaneInquest)

void FArcaneInquestModule::StartupModule()
{
	UE_LOG(LogArcaneInquest, Log, TEXT("Starting ArcaneInquest module."));
}

void FArcaneInquestModule::ShutdownModule()
{
	UE_LOG(LogArcaneInquest, Log, TEXT("Shutting down ArcaneInquest module."));
}
