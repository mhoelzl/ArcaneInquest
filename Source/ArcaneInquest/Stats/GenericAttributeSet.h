// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "AttributeSet.h"
#include "GenericAttributeSet.generated.h"

/**
 * The attributes of actors that are relevant for gameplay effects. Not every attribute is applicable to every actor, e.g., towers or robots can't bleed and minions have only basic attacks and therefore cannot use mana.
 */
UCLASS()
class ARCANEINQUEST_API UGenericAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	/** The maximal health this actor has. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats",
		// Gameplay effects can't modify MaxHealth directly
		meta = (HideFromModifiers))
	float MaxHealth;

	/** The current health of the actor */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats",
		// Gameplay effect can't modify Health directly 
		meta = (HideFromModifiers))
	float Health;

	/** Since gameplay effects can't modify health directly, they have to modify the damage they want to apply to the actor, which will then be used to update the health values. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats",
		// Gameplay cannot be 'powered' by Damage since it is transient
		meta = (HideFromLevelInfos))
	float DamageReceived;

	/** Mana is used to power abilities, both physical and energy-based. This it the maximal amount of mana an actor has available. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float MaxMana;

	/** The current amount of mana the actor has available. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float Mana;

	/** The energy damage for this actor. It powers energy-damage-based GameplayEffects. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float EnergyDamage;

	/** The physical damage for this actor. It powers physical-damage-based GameplayEffects */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float PhysicalDamage;

	/** The probability that an attack of this actor is a critical attack that does more damage. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float CritChance;

	/** The multiplier for the attack damage of critical hits. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float CritMultiplier;

	/** The amount of energy damage reduction provided by armor or other buffs. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float EnergyArmorDamageReduction;
	
	/** The amount of physical damage reduction provided by armor or other buffs. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float PhysicalArmorDamageReduction;

	/** The chance that the actor dodges an attack. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float DodgeChance;

	/** The percentage of damage that the actor gets back when hitting enemies. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float LifeSteal;

	/** The strength of the actor. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
	float Strength;

	/** A stacking attribute that applies damage over time. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float Bleed;

	/** A stacking attribute that applies damage over time. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float Burn;

	/** A stacking attribute that applies damage over time. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float Freeze;

	/** A non-stacking attribute that reduces the movement speed of the actor. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float SlowDown;
	
	/** A non-stacking attribute that increases the movement speed of the actor. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float SpeedUp;

	virtual bool PreGameplayEffectExecute(struct FGameplayEffectModCallbackData &Data) override;
	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;
	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator> &) const override;
};

#define INIT(Name) Name{ FindFieldChecked<UProperty>(UGenericAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UGenericAttributeSet, Name)) }

struct FGenericAttributeSetProperties
{
	UProperty* MaxHealth;
	UProperty* Health;
	UProperty* DamageReceived;
	UProperty* MaxMana;
	UProperty* Mana;
	UProperty* EnergyDamage;
	UProperty* PhysicalDamage;
	UProperty* CritChance;
	UProperty* CritMultiplier;
	UProperty* EnergyArmorDamageReduction;
	UProperty* PhysicalArmorDamageReduction;
	UProperty* DodgeChance;
	UProperty* LifeSteal;
	UProperty* Strength;
	UProperty* Bleed;
	UProperty* Burn;
	UProperty* Freeze;
	UProperty* SlowDown;
	UProperty* SpeedUp;

	FGenericAttributeSetProperties()
		: INIT(MaxHealth)
		, INIT(Health)
		, INIT(DamageReceived)
		, INIT(MaxMana)
		, INIT(Mana)
		, INIT(EnergyDamage)
		, INIT(PhysicalDamage)
		, INIT(CritChance)
		, INIT(CritMultiplier)
		, INIT(EnergyArmorDamageReduction)
		, INIT(PhysicalArmorDamageReduction)
		, INIT(DodgeChance)
		, INIT(LifeSteal)
		, INIT(Strength)
		, INIT(Bleed)
		, INIT(Burn)
		, INIT(Freeze)
		, INIT(SlowDown)
		, INIT(SpeedUp)
	{
	}
};

#undef INIT
