// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"
#include "GameplayTagsModule.h"
#include "GenericAttributeSet.h"

bool UGenericAttributeSet::PreGameplayEffectExecute(struct FGameplayEffectModCallbackData &Data)
{
	static FGenericAttributeSetProperties Properties;
	UProperty* ModifiedProperty{ Data.EvaluatedData.Attribute.GetUProperty() };

	// Return false in cases where the modification should not take place.
	return true;
}

void UGenericAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData &Data)
{
	static FGenericAttributeSetProperties Properties;
	UProperty* ModifiedProperty{ Data.EvaluatedData.Attribute.GetUProperty() };

	if (ModifiedProperty == Properties.DamageReceived)
	{
		// TODO: Take into account armor, etc.
		Health -= DamageReceived;
		DamageReceived = 0.f;

		static auto FireTag{ IGameplayTagsModule::RequestGameplayTag(FName(TEXT("FireDamage"))) };
		if (Data.EffectSpec.CapturedSourceTags.GetAggregatedTags()->HasTag(FireTag, EGameplayTagMatchType::IncludeParentTags, EGameplayTagMatchType::Explicit))
		{
			// Possibly apply fire damage
		}
		// Check whether actor has died.
	}
}

void UGenericAttributeSet::GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const
{
	DOREPLIFETIME(UGenericAttributeSet, MaxHealth);
	DOREPLIFETIME(UGenericAttributeSet, Health);
	DOREPLIFETIME(UGenericAttributeSet, DamageReceived);
	DOREPLIFETIME(UGenericAttributeSet, MaxMana);
	DOREPLIFETIME(UGenericAttributeSet, Mana);
	DOREPLIFETIME(UGenericAttributeSet, EnergyDamage);
	DOREPLIFETIME(UGenericAttributeSet, PhysicalDamage);
	DOREPLIFETIME(UGenericAttributeSet, CritChance);
	DOREPLIFETIME(UGenericAttributeSet, CritMultiplier);
	DOREPLIFETIME(UGenericAttributeSet, EnergyArmorDamageReduction);
	DOREPLIFETIME(UGenericAttributeSet, PhysicalArmorDamageReduction);
	DOREPLIFETIME(UGenericAttributeSet, DodgeChance);
	DOREPLIFETIME(UGenericAttributeSet, LifeSteal);
	DOREPLIFETIME(UGenericAttributeSet, Strength);
	DOREPLIFETIME(UGenericAttributeSet, Bleed);
	DOREPLIFETIME(UGenericAttributeSet, Burn);
	DOREPLIFETIME(UGenericAttributeSet, Freeze);
	DOREPLIFETIME(UGenericAttributeSet, SlowDown);
	DOREPLIFETIME(UGenericAttributeSet, SpeedUp);
}
