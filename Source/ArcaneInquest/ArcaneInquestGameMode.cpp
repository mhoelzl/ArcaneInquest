// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "ArcaneInquest.h"
#include "ArcaneInquestGameMode.h"
#include "ArcaneInquestPlayerController.h"
#include "Units/PlayerCharacter.h"

AArcaneInquestGameMode::AArcaneInquestGameMode()
{
	PlayerControllerClass = AArcaneInquestPlayerController::StaticClass();
	DefaultPawnClass = APlayerCharacter::StaticClass();
}
