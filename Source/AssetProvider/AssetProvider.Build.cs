// Copyright 2016 Dr. Matthias H�lzl. All Rights Reserved.

using UnrealBuildTool;

public class AssetProvider : ModuleRules
{
	public AssetProvider(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "GameplayTags", "AIModule", "GameplayAbilities" });
	}
}
