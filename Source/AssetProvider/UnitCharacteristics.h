// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once
#include "GenericTeamAgentInterface.h"

namespace FTeams {
	static FGenericTeamId RedTeam{ 0u };
	static FGenericTeamId BlueTeam{ 1u };
};

/**
 * The combat style preferred by a character. Potentially influences its stats and abilities.
 */
UENUM()
enum class ECombatStyle : uint8
{
	// Unknown or non-combat characters
	None,
	// Non-magical styles
	Melee,
	Ranged,
	// Magical styles
	// A mage focused on dealing damage
	WarMage,
	// A mage focused on manipulating the environment
	Conjurer,
	// A mage focused on support and healing
	Healer
};

/**
 * What kind of character this is.
 */
UENUM()
enum class EUnitKind : uint8
{
	// A character containing some kind of misconfiguration, e.g. a direct instance of AbilityCharacter
	None,
	// A computer-controlled character
	Minion,
	// A human-controlled character
	Player
};
