// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "AssetProvider.h"
// Required for UGameplayAbilitySet
#include "Engine/DataAsset.h"
#include "GameplayAbilitySet.h"
#include "UnitFinder.h"

UUnitFinder::UUnitFinder(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinder<USkeletalMesh> PlayerMeshFinder;
		ConstructorHelpers::FClassFinder<UAnimInstance> PlayerAnimClassFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> PlayerMaterialFinder;
		ConstructorHelpers::FObjectFinder<UGameplayAbilitySet> PlayerAbilityFinder;
		ConstructorHelpers::FObjectFinder<USkeletalMesh> WarriorMeshFinder;
		ConstructorHelpers::FClassFinder<UAnimInstance> WarriorAnimClassFinder;
		ConstructorHelpers::FObjectFinder<UGameplayAbilitySet> WarriorAbilityFinder;
		ConstructorHelpers::FObjectFinder<USkeletalMesh> MageMeshFinder;
		ConstructorHelpers::FClassFinder<UAnimInstance> MageAnimClassFinder;
		ConstructorHelpers::FObjectFinder<UGameplayAbilitySet> MageAbilityFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> RedWarriorMaterialFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> RedMageMaterialFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> BlueWarriorMaterialFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> BlueMageMaterialFinder;
		ConstructorHelpers::FObjectFinder<UMaterialInstance> TeamlessMaterialFinder;

		FConstructorStatics()
			: PlayerMeshFinder{ TEXT("SkeletalMesh'/Game/ArcaneInquest/Units/Meshes/Player_SM.Player_SM'") }
			, PlayerAnimClassFinder{ TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C'") }
			, PlayerMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/Player_Body_MI.Player_Body_MI'") }
			, PlayerAbilityFinder{ TEXT("GameplayAbilitySet'/Game/ArcaneInquest/Abilities/GenericGameplayAbilitySet.GenericGameplayAbilitySet'")}
			, WarriorMeshFinder{ TEXT("SkeletalMesh'/Game/ArcaneInquest/Units/Meshes/Minion_SM.Minion_SM'") }
			, WarriorAnimClassFinder{ TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C'") }
			, WarriorAbilityFinder{ TEXT("GameplayAbilitySet'/Game/ArcaneInquest/Abilities/GenericGameplayAbilitySet.GenericGameplayAbilitySet'") }
			, MageMeshFinder{ TEXT("SkeletalMesh'/Game/ArcaneInquest/Units/Meshes/Minion_SM.Minion_SM'") }
			, MageAnimClassFinder{ TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C'") }
			, MageAbilityFinder{ TEXT("GameplayAbilitySet'/Game/ArcaneInquest/Abilities/GenericGameplayAbilitySet.GenericGameplayAbilitySet'") }
			, RedWarriorMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/WarriorMinion_Red_Body_MI.WarriorMinion_Red_Body_MI'") }
			, RedMageMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/MageMinion_Red_Body_MI.MageMinion_Red_Body_MI'") }
			, BlueWarriorMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/WarriorMinion_Blue_Body_MI.WarriorMinion_Blue_Body_MI'") }
			, BlueMageMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/MageMinion_Blue_Body_MI.MageMinion_Blue_Body_MI'") }
			, TeamlessMaterialFinder{ TEXT("MaterialInstanceConstant'/Game/ArcaneInquest/Units/Materials/Minion_Body_MI.Minion_Body_MI'") }
		{
			ensure(PlayerMeshFinder.Succeeded());
			ensure(PlayerAnimClassFinder.Succeeded());
			ensure(PlayerAbilityFinder.Succeeded());
			ensure(PlayerMaterialFinder.Succeeded());
			ensure(WarriorMeshFinder.Succeeded());
			ensure(WarriorAnimClassFinder.Succeeded());
			ensure(WarriorAbilityFinder.Succeeded());
			ensure(MageMeshFinder.Succeeded());
			ensure(MageAnimClassFinder.Succeeded());
			ensure(MageAbilityFinder.Succeeded());
			ensure(RedWarriorMaterialFinder.Succeeded());
			ensure(RedMageMaterialFinder.Succeeded());
			ensure(BlueWarriorMaterialFinder.Succeeded());
			ensure(BlueMageMaterialFinder.Succeeded());
			ensure(TeamlessMaterialFinder.Succeeded());
		};
	};
	static FConstructorStatics ConstructorStatics;
	static FVector Location{ FVector(0.f, 0.f, -88.f) };
	static FRotator Rotation{ FRotator(0.f, 270.f, 0.f) };
	static FVector2D CapsuleSize{ FVector2D(35.f, 88.f) };

	DefaultPlayerData = FUnitInitData(ECombatStyle::Conjurer,  ConstructorStatics.PlayerMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.PlayerAnimClassFinder.Class, ConstructorStatics.PlayerMaterialFinder.Object, ConstructorStatics.PlayerAbilityFinder.Object);
	BlueWarriorData = FUnitInitData(ECombatStyle::Melee, ConstructorStatics.WarriorMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.WarriorAnimClassFinder.Class, ConstructorStatics.BlueWarriorMaterialFinder.Object, ConstructorStatics.WarriorAbilityFinder.Object);
	RedWarriorData = FUnitInitData(ECombatStyle::Melee, ConstructorStatics.WarriorMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.WarriorAnimClassFinder.Class, ConstructorStatics.RedWarriorMaterialFinder.Object, ConstructorStatics.WarriorAbilityFinder.Object);
	BlueMageData = FUnitInitData(ECombatStyle::Conjurer, ConstructorStatics.MageMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.MageAnimClassFinder.Class, ConstructorStatics.BlueMageMaterialFinder.Object, ConstructorStatics.MageAbilityFinder.Object);
	RedMageData = FUnitInitData(ECombatStyle::Conjurer, ConstructorStatics.MageMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.MageAnimClassFinder.Class, ConstructorStatics.RedMageMaterialFinder.Object, ConstructorStatics.MageAbilityFinder.Object);
	TeamlessWarriorData = FUnitInitData(ECombatStyle::Melee, ConstructorStatics.WarriorMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.WarriorAnimClassFinder.Class, ConstructorStatics.TeamlessMaterialFinder.Object, ConstructorStatics.WarriorAbilityFinder.Object);
	TeamlessMageData = FUnitInitData(ECombatStyle::Conjurer, ConstructorStatics.MageMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.MageAnimClassFinder.Class, ConstructorStatics.TeamlessMaterialFinder.Object, ConstructorStatics.MageAbilityFinder.Object);
	UnknownMinionData = FUnitInitData(ECombatStyle::None, ConstructorStatics.MageMeshFinder.Object, Location, Rotation, CapsuleSize, ConstructorStatics.MageAnimClassFinder.Class, ConstructorStatics.TeamlessMaterialFinder.Object, nullptr);
}


const FUnitInitData UUnitFinder::GetUnitData(EUnitKind UnitType, FGenericTeamId TeamId, ECombatStyle InCombatStyle) const
{
	FUnitInitData Result;
	switch (UnitType)
	{
	case EUnitKind::None:
		Result = UnknownMinionData;
	case EUnitKind::Minion:
		Result = GetMinionData(TeamId, InCombatStyle);
		break;
	default:
		Result = GetPlayerData(UnitType);
		break;
	}
	Result.CombatStyle = InCombatStyle;
	return Result;
}

const FUnitInitData& UUnitFinder::GetPlayerData(EUnitKind UnitType) const
{
	switch (UnitType)
	{
		case EUnitKind::Player:
			return DefaultPlayerData;	
		default:
			UE_LOG(LogAssetProvider, Warning, TEXT("Unknown player character type. Using default character."));
			return DefaultPlayerData;
	}
}

const FUnitInitData& UUnitFinder::GetMinionData(FGenericTeamId TeamId, ECombatStyle CombatStyle) const
{
	switch (CombatStyle)
	{
	case ECombatStyle::None:
		return UnknownMinionData;
	// Warriors
	case ECombatStyle::Melee:
	case ECombatStyle::Ranged:
		if (TeamId == FTeams::RedTeam)
		{
			return RedWarriorData;
		}
		else if (TeamId == FTeams::BlueTeam)
		{
			return BlueWarriorData;
		}
		return TeamlessWarriorData;
	// Mages
	case ECombatStyle::WarMage:
	case ECombatStyle::Conjurer:
	case ECombatStyle::Healer:
		if (TeamId == FTeams::RedTeam)
		{
			return RedMageData;
		}
		else if (TeamId == FTeams::BlueTeam)
		{
			return BlueMageData;
		}
		return TeamlessMageData;
	default:
		UE_LOG(LogAssetProvider, Warning, TEXT("Trying to create minion with unknown properties: %d, %d"), TeamId.GetId(), static_cast<int>(CombatStyle));
		return UnknownMinionData;
	}
}
