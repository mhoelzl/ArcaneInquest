// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "EngineMinimal.h"
#include "ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAssetProvider, All, All);

class UUnitFinder;
class UDataTableFinder;

class ASSETPROVIDER_API FAssetProviderModule : public FDefaultGameModuleImpl
{
public:
	FAssetProviderModule();

	virtual void StartupModule() override;

	virtual void ShutdownModule() override;

	virtual UUnitFinder* GetUnitFinder();
	virtual UDataTableFinder* GetDataTableFinder();

	static FAssetProviderModule& Get();

protected:
	UUnitFinder* UnitFinder;
	UDataTableFinder* DataTableFinder;
};
