// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "AssetProvider.h"
#include "DataTableFinder.h"

UDataTableFinder::UDataTableFinder(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinder<UDataTable> GameplayTagsTableFinder;
		ConstructorHelpers::FObjectFinder<UDataTable> GenericAttributesDataTableFinder;

		FConstructorStatics()
			: GameplayTagsTableFinder(TEXT("DataTable'/Game/ArcaneInquest/Abilities/GameplayTags.GameplayTags'"))
			, GenericAttributesDataTableFinder(TEXT("DataTable'/Game/ArcaneInquest/Stats/GenericAttributeMetaData.GenericAttributeMetaData'"))
		{
			ensure(GameplayTagsTableFinder.Succeeded());
			ensure(GenericAttributesDataTableFinder.Succeeded());
		}
	};

	static FConstructorStatics ConstructorStatics;
	GameplayTagsTable = ConstructorStatics.GameplayTagsTableFinder.Object;
	GenericAttributesDataTable = ConstructorStatics.GenericAttributesDataTableFinder.Object;
}
