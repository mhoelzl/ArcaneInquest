// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "Engine/DataTable.h"
#include "DataTableFinder.generated.h"

/**
 * Provide access to all data tables.
 */
UCLASS()
class ASSETPROVIDER_API UDataTableFinder : public UObject
{
	GENERATED_BODY()

public:
	UDataTableFinder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UDataTable* GetGameplayTagsTable() const
	{
		return GameplayTagsTable;
	}

	UDataTable* GetGenericAttributesDataTable() const
	{
		return GenericAttributesDataTable;
	};

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DataTables", Meta = (AllowPrivateAccess = "true"))
	UDataTable* GameplayTagsTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DataTables", Meta = (AllowPrivateAccess = "true"))
	UDataTable* GenericAttributesDataTable;
};
