// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#pragma once

#include "Engine.h"
#include "Animation/AnimInstance.h"
#include "UnitCharacteristics.h"
#include "UnitFinder.generated.h"

class UGameplayAbilitySet;

/**
 * The data for a single unit type, e.g., a particular player character or a minion type.
 */
USTRUCT()
struct ASSETPROVIDER_API FUnitInitData
{
	GENERATED_BODY()

	ECombatStyle CombatStyle;
	USkeletalMesh* Mesh;
	FVector Location;
	FRotator Rotation;
	FVector2D CapsuleSize;
	TSubclassOf<UAnimInstance> AnimInstance;
	UMaterialInstance* Material;
	UGameplayAbilitySet* Abilities;

	FUnitInitData(ECombatStyle InCombatStyle, USkeletalMesh* InMesh, FVector InTranslation, FRotator InRotation, FVector2D InCapsuleSize, TSubclassOf<UAnimInstance> InAnimInstance, UMaterialInstance* InMaterial, UGameplayAbilitySet* InAbilities)
		: CombatStyle(InCombatStyle)
		, Mesh(InMesh)
		, Location(InTranslation)
		, Rotation(InRotation)
		, CapsuleSize(InCapsuleSize)
		, AnimInstance(InAnimInstance)
		, Material(InMaterial)
		, Abilities(InAbilities)
	{
	};

	FUnitInitData(const FUnitInitData& UnitData)
		: CombatStyle(UnitData.CombatStyle)
		, Mesh(UnitData.Mesh)
		, Location(UnitData.Location)
		, Rotation(UnitData.Rotation)
		, CapsuleSize(UnitData.CapsuleSize)
		, AnimInstance(UnitData.AnimInstance)
		, Material(UnitData.Material)
		, Abilities(UnitData.Abilities)
	{
	};

	FUnitInitData()
		: CombatStyle(ECombatStyle::None)
		, Mesh(nullptr)
		, Location(FVector::ZeroVector)
		, Rotation(FRotator::ZeroRotator)
		, CapsuleSize(FVector2D::ZeroVector)
		, AnimInstance(nullptr)
		, Material(nullptr)
		, Abilities(nullptr)
	{
	};
};

/**
 * The class used for finding Unit meshes and animations.
 */
UCLASS()
class ASSETPROVIDER_API UUnitFinder : public UObject
{
	GENERATED_BODY()

public:
	UUnitFinder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/** The general function for obtaining unit data. Delegates to the specialized functions for players and minions according to its first element. */
	UFUNCTION(BlueprintPure, Category = "Units")
	const FUnitInitData GetUnitData(EUnitKind UnitType, FGenericTeamId TeamId, ECombatStyle CombatStyle) const;

	/** The function used for getting unit data for player characters only. */
	UFUNCTION(BlueprintPure, Category = "Units")
	const FUnitInitData& GetPlayerData(EUnitKind UnitType) const;

	/** The function for getting unit data for minions. */
	UFUNCTION(BlueprintPure, Category = "Units")
	const FUnitInitData& GetMinionData(FGenericTeamId TeamId, ECombatStyle CombatStyle) const;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData DefaultPlayerData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData BlueWarriorData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData BlueMageData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData RedWarriorData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData RedMageData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData TeamlessWarriorData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData TeamlessMageData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prototypes", Meta = (AllowPrivateAccess = "true"))
	FUnitInitData UnknownMinionData;
};
