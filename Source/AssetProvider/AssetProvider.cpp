// Copyright 2016 Dr. Matthias Hölzl. All Rights Reserved.

#include "AssetProvider.h"
#include "EngineGlobals.h"
#include "ModuleManager.h"
#include "UnitFinder.h"
#include "DataTableFinder.h"

IMPLEMENT_GAME_MODULE(FAssetProviderModule, AssetProvider) ;

DEFINE_LOG_CATEGORY(LogAssetProvider)

FAssetProviderModule::FAssetProviderModule()
	: FDefaultGameModuleImpl()
	, UnitFinder{ nullptr }
{
}

void FAssetProviderModule::StartupModule()
{
	FDefaultGameModuleImpl::StartupModule();
	UE_LOG(LogAssetProvider, Log, TEXT("Starting AssetProvider module."));

	UObject* Outer = GetTransientPackage();
	check(Outer);
	UnitFinder = NewObject<UUnitFinder>(Outer, FName(TEXT("MinionFinder")));
	UnitFinder->AddToRoot();
	DataTableFinder = NewObject<UDataTableFinder>(Outer, FName(TEXT("DataTableFinder")));
	DataTableFinder->AddToRoot();
}

void FAssetProviderModule::ShutdownModule()
{
	FDefaultGameModuleImpl::ShutdownModule();

	UE_LOG(LogAssetProvider, Log, TEXT("Shutting down AssetProvider module."));
	if (UnitFinder && UnitFinder->IsValidLowLevel())
	{
		UnitFinder->ConditionalBeginDestroy();
		UnitFinder = nullptr;
	}
	if (DataTableFinder && DataTableFinder->IsValidLowLevel())
	{
		DataTableFinder->ConditionalBeginDestroy();
		DataTableFinder = nullptr;
	}
}

UUnitFinder* FAssetProviderModule::GetUnitFinder()
{
	checkf(UnitFinder, TEXT("AssetProvider not initialized."));
	return UnitFinder;
}

UDataTableFinder* FAssetProviderModule::GetDataTableFinder()
{
	checkf(DataTableFinder, TEXT("AssetProvider not initialized."));
	return DataTableFinder;
}

FAssetProviderModule& FAssetProviderModule::Get()
{
	return FModuleManager::LoadModuleChecked<FAssetProviderModule>("AssetProvider");
}
